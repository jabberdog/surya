# [Surya Heating](http://www.suryaheating.co.uk)

Magento: 1.9.2.0

## Transactional Emails
To set up emails in magento, you need to add templates in the transactional emails section of the ui.
This section can be located at `System -> Transactional Emails`.

The idea is that you choose a predefined template and then edit it as you please. Until a template for the
transaction is defined it will always use the default email (which sends everything as if the user is **default**).

Yireo has some information regarding this: [Read Here](https://www.yireo.com/tutorials/magento/magento-theming/1670-customizing-magento-email-templates).