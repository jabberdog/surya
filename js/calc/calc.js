/**
 * Created by ben on 10/12/16.
 */
jQuery(function($) {
    //HTML for calculator popup
    var htmlblock = '<div id="calc-wrap"><div id="calc-main" class="col-xs-12 col-md-8 col-lg-6"><i id="close-calc" class="fa fa-window-close-o" aria-hidden="true"></i><div class="col-xs-12"><h2 class="yourreqs centered-white">Calculate your IR heating requirements</h2></div><div class="col-xs-12 col-md-8 col-md-offset-2 text-center property-wrap"><h3 class="centered-white" id="property-text">How well insulated is your property?</h3><div id="img-block-old" class="col-xs-4 calc-img-select"><img id="oldInactive" class="img-responsive house-inactive" src="https://suryaheating.co.uk/media/wysiwyg/Surya/housetype/older.png"><img id="oldActive" class="img-responsive house-active" src="https://suryaheating.co.uk/media/wysiwyg/Surya/housetype/olderActive.png"></div><div class="col-xs-4 calc-img-select" id="img-block-modern"><img id="modernInactive" class="img-responsive house-inactive" src="https://suryaheating.co.uk//media/wysiwyg/Surya/housetype/modern.png"><img id="modernActive" class="img-responsive house-active" src="https://suryaheating.co.uk//media/wysiwyg/Surya/housetype/modernActive.png"></div><div class="col-xs-4 calc-img-select" id="img-block-new"><img id="newInactive" class="img-responsive house-inactive" src="https://suryaheating.co.uk//media/wysiwyg/Surya/housetype/new.png"><img id="newActive" class="img-responsive house-active" src="https://suryaheating.co.uk//media/wysiwyg/Surya/housetype/newActive.png"></div></div><div id="area-wrap" class="col-xs-12 col-md-8 col-md-offset-2 text-center"><div class="col-xs-12"><div id="area-text-wrap"><h3 id="area-text" class="centered-white">Enter room size in</h3><select id="units_mf"><option value="1">meters</option><option value="3.2808">feet</option></select></div></div><div class="col-xs-4"><div class="selector"><p class="centered-white no-margin">Room Length</p><input type="number" id="input-a" min=0  name="x" /></div></div><div class="col-xs-4"><div class="selector"><p class="centered-white no-margin">Room Width</p><input  type="number" id="input-b" min=0  name="y" /></div></div><div class="col-xs-4"><div class="selector"><p class="centered-white no-margin">Room Height</p><input type="number" id="input-c" min=0  name="z" /></div></div></div><div class="col-xs-12"><div id="calc-button-wrap"><button>Calculate!</button></div></div><div class="col-xs-12"><h3 id="output-text" class="centered-white"></h3><h3 id="output-watts" class="centered-white"></h3></div><p style="float:right;text-align:center;width:100%;font-weight: bold"><i class="fa fa-phone" style="margin-right:3px" aria-hidden="true"></i>Need Help Deciding?<br>Call Our Experts On 0116-214-124</p></div></div>';
    //Instert html before .page so it's 100% height will fill the whole screen
    $( ".page").prepend(htmlblock);
    $(".calctrig").click(function(){
        $("#calc-wrap").show();
    })
    var multi = 0;
    var meters = true;
    var wattage = parseInt($("#wattage-holder").text());

    function roundit(myNumber){
        if(myNumber<1.3){
            return Math.floor(myNumber);
        }
        else {
            return Math.ceil(myNumber);
        }
        document.getElementById("main").innerHTML="test";
    }

    var length, width, height;
    $(document).ready(function() {
        $("#calctrigger").click(function (e) {
            e.preventDefault();
            $("#calc-wrap").fadeIn(300, function () {
                $(this).focus();
            });
        });
        $('#calc-wrap').click(function () {
            $(this).fadeOut(300);
        });
        $('#close-calc').click(function () {
            $('#calc-wrap').fadeOut(300);
        });
        $("#calc-main").click(function(e){
            e.stopPropagation();
        });
        $('#oldInactive').click(function(){
            multi = 30;
            $('.house-active').hide();
            $('.house-inactive').show();
            $(this).fadeOut(20, function(){
                $('#oldActive').fadeIn(250)
            })
        })
        $('#modernInactive').click(function(){
            multi = 25;
            $('.house-active').hide();
            $('.house-inactive').show();
            $(this).fadeOut(20, function(){
                $('#modernActive').fadeIn(250)
            })
        })
        $('#newInactive').click(function(){
            multi = 20;
            $('.house-active').hide();
            $('.house-inactive').show();
            $(this).fadeOut(20, function(){
                $('#newActive').fadeIn(250)
            })
        })
        $("button").click(function(){
            var inputOK = true;
            $(".property-wrap").css("border","none");
            $("#area-wrap").css("border","none");
            $("#property-text").css("color","white");
            $("#area-text").css("color","white");
            if (multi === 0){
                inputOK = false;
                $(".property-wrap").css("border","3px solid #F3742C");
                $("#property-text").css("color","#F3742C");
            }
            if(!$("#input-a").val().length > 0 || !$("#input-b").val().length > 0 || !$("#input-c").val().length > 0){
                inputOK = false;
                $("#area-wrap").css("border","3px solid #F3742C");
                $("#area-text").css("color","#F3742C");
            }
            else{
                var length = $("#input-a").val();
                var width = $("#input-b").val()
                var height = $("#input-c").val();
                if(length < 1 || width < 1 || height < 1){
                    inputOK = false;
                    $("#area-wrap").css("border","3px solid orange");
                    $("#area-text").css("color","#F3742C");
                }
            }
            if(inputOK){
                var unit_multiplier = $( "#units_mf" ).val();
                length = length / unit_multiplier;
                height = height / unit_multiplier;
                width = width / unit_multiplier;
                cubic_meters = (length * height * width);
                var required_watts = Math.round(cubic_meters * multi,2);
                var required_pans = roundit(required_watts/wattage);
                if(required_pans < 1){required_pans = 1}
                $( "#output-text" ).text('Heating your room will require '+required_watts+' Watts');
                $( "#output-watts").text("You'll need "+required_pans+" of these "+wattage+"W panels.");
                $( "#output-text").prepend('<i class="fa fa-circle" style="margin-right:5px" aria-hidden="true"></i>');
                $( "#output-watts").prepend('<i class="fa fa-circle" style="margin-right:5px" aria-hidden="true"></i>');


            }
        });
    });
});




